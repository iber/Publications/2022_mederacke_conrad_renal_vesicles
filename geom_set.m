%% load stage 1 and create file for a later stage
model=mphload('L_stage.mph');


%% get file name of the current file with epithlium geom
% mes
tbl_name=model.component("comp1").geom("geom1").feature("ic1").getString('filename')
tbl_name=char(tbl_name); % convert to a string from java string
cut_pos=strfind(tbl_name,'_1.txt'); %get position in the string of _1.txt
tbl_path_mes = tbl_name(1:cut_pos)

% epith
tbl_name=model.component("comp1").geom("geom1").feature("ic2").getString('filename')
tbl_name=char(tbl_name); % convert to a string from java string
cut_pos=strfind(tbl_name,'_1.txt'); %get position in the string of _1.txt
tbl_path_ep = tbl_name(1:cut_pos)




% displ® epi
tbl_name = model.func('int2').getString('filename')
tbl_name = char(tbl_name); % convert to a string from java string
cut_pos = strfind(tbl_name,'_1.txt'); %get position in the string of _1.txt
tbl_path_displ = tbl_name(1:cut_pos)

% displ® mes
tbl_name_mes = model.func('int1').getString('filename')
tbl_name_mes = char(tbl_name_mes); % convert to a string from java string
cut_pos_mes = strfind(tbl_name_mes,'_1.txt'); %get position in the string of _1.txt
tbl_path_displ_mes = tbl_name_mes(1:cut_pos_mes)

%Load all files with masked RV's
RV_files = natsortfiles(dir('../RV/Boundaries/*.txt'))
RV_names = {RV_files(:).name}
stage_RV = []
for i = 1:length(RV_names)
    splt = strsplit(string(RV_names(i)),'t')
    splt = regexp(splt(2),'\d*','match')
    stage_RV(i) = splt
end  
model.result().export().create('Wnt9_growth', "Animation");

%exclude distortion on stalk
exV = 0
if exV == 1
    
    load  /Volumes/iber/Users/Malte_Mederacke/Kidney_Comsol_geometries/2D_new/Control_48h/UB/Bound_Aligned_Smooth/Bound_Aligned_SmoothC.mat;
    epi = boundSmoothC

     
    load /Volumes/iber/Users/Malte_Mederacke/Kidney_Comsol_geometries/2D_new/Control_48h/Mesenchyme/Bound_Aligned_Smooth/Bound_Aligned_SmoothC.mat;
    mes = boundSmoothC

    load /Volumes/iber/Users/Malte_Mederacke/Kidney_Comsol_geometries/2D_new/Control_48h/tot_geom/Bound_Aligned_Smooth/tot_bound.mat
    tot = tot_bound
end



%select stage

for n_stage=1:1:48  % current stage
%    print(n_stage)

%% import kidney geometry and displ field


tbl_nameS=[tbl_path_mes num2str(n_stage) '.txt']
model.geom('geom1').feature('ic1').set('filename',tbl_nameS);

tbl_nameS=[tbl_path_ep num2str(n_stage) '.txt']
model.geom('geom1').feature('ic2').set('filename',tbl_nameS);
% model.geom('geom1').feature('ic1').importToTable;

tbl_nameS=[tbl_path_mes num2str(n_stage) '.txt']
model.geom('geom1').feature('ic3').set('filename',tbl_nameS);


%import distortion matrix for epithelium
model.func('int1').discardData;
tbl_nameS=[tbl_path_displ_mes num2str(n_stage) '.txt']
model.func('int1').set('filename',tbl_nameS);
model.func('int1').set('nargs',2);
model.func('int1').importData;

% import distortion matrix for mesenchyme
model.func('int2').discardData;
tbl_nameS=[tbl_path_displ num2str(n_stage) '.txt']
model.func('int2').set('filename',tbl_nameS);
model.func('int2').set('nargs',2);
model.func('int2').importData;




%% run
fig=figure('visible','off');
mphgeom(model,"geom1");
model.component("comp1").geom("geom1").run();

%% set file names and solver setting
titlestr=['stage'  int2str(n_stage) '_out.mph'] ;
% model.study('std1').set('batchfile',titlestr); %out file name
model.study('std1').run
% model.batch('b1').set('batchfile',titlestr);%out file name
% model.batch('c1').set('nn',16); %#number of nodes
% catch
% end
%% save files and figures
titlestr=['./stage'  int2str(n_stage) '/WT_in.mph'];
mphsave(model,titlestr);

titlestr=['stage'  int2str(n_stage)];
mkdir(titlestr);

titlestr=['./stage'  int2str(n_stage) '/stage'  int2str(n_stage) '_geom.tif'];
print(fig, '-dpng', titlestr);

%titlestr=['./stage'  int2str(n_stage) '/stage'  int2str(n_stage) '_in.mph'];


%Plot Wnt9b with manually marked RV

fig=figure('visible','off');
mphplot(model);

if isempty(RV_names(logical(ismember(stage_RV,n_stage))))== 0
    rvmarks= load(strcat('../RV/Boundaries/',string(RV_names(logical(ismember(stage_RV,n_stage))))))
    hold on
    model.result('pg1').run;
    mphplot(model,('pg1'),'rangenum',1);
    scatter(rvmarks(:,1),rvmarks(:,2),'filled','black')
    hold off
else model.result('pg1').run
     mphplot(model,('pg1'),'rangenum',1);

end

titlestr=['./stage'  int2str(n_stage) '/stage'  int2str(n_stage) '_WNT9b.tif'];
print(fig, '-dpng', titlestr);


% Animate growth
savedir=['/Volumes/iber/Users/Malte_Mederacke/Kidney_Comsol_geometries/2D_new/Control_48h/mph_files/','stage', int2str(n_stage) '/animations_frames']
mkdir(savedir)

model.result.export('Wnt9_growth').set('type','imageseq')
model.result().export("Wnt9_growth").set("plotgroup", "pg1");
model.result().export("Wnt9_growth").set("synchronize", false);

titlestr= ['./stage'  int2str(n_stage) '/animations_frames/WNT9b_frames.png'];
model.result().export('Wnt9_growth').set("imagefilename", titlestr);

model.result().export("Wnt9_growth").set("framesel", "all");
model.result().export("Wnt9_growth").set("timeinterp", true);
model.result().export("Wnt9_growth").set("solnumtype", "inner");
model.result().export("Wnt9_growth").set("type", "imageseq");
model.result().export("Wnt9_growth").set("alwaysask", false);
model.result().export("Wnt9_growth").set("looplevelinput", "interp");

model.result().export("Wnt9_growth").set("interp", "range(0,200,7200)");
model.result().export("Wnt9_growth").set("sweeptype", "solutions");
model.result().export("Wnt9_growth").set("maxframes", 10);


end