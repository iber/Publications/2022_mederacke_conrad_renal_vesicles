#measure
import pandas as pd
import numpy as np
import plotly.express as px
import plotly.io as pio
import plotly.graph_objects as go
import scipy.signal as sp
from matplotlib import pyplot as plt

from skimage.measure import regionprops_table
from scipy.spatial import KDTree
# import open3d as o3d

def measure_features(labels, measurement, distances = False, surface = None,id_from_pointcloud = None):
    #input: (list of)labels and measurements that are to be evaluated. 
    #output: dataframe with image ID, label ID and measurements
    properties_table  = pd.DataFrame()
    if type(labels) == list:
        pass
    else: 
        labels = [labels]
    rep = pd.DataFrame()
    for i in range(len(labels)):
        rp_table = regionprops_table(labels[i], properties=measurement)
        rp_table['image']=np.array(rp_table['label'].max()*[i+1])
        rp_table = pd.DataFrame(rp_table)
        properties_table = properties_table.append(rp_table)
        
        #add parameter swept for and expression names for each image
        if id_from_pointcloud is not None:
            data = id_from_pointcloud.drop(['X','Y','Z'],axis = 1)
            data = data.columns.str.split(',')
            var_name = [data[var][0] for var in range(len(data))]
            par = [data[par][1:len(data[par])] for par in range(len(data))]
            val = []
            for j in range(len(par)):
                val.append([val.split('=')[1] for val in par[j]])
            par = [val.split('=')[0] for val in par[i]]
            parameters = pd.DataFrame(val, columns=par)
            parameters['variable'] = var_name
            rep = rep.append(properties_table[properties_table['image'] == i + 1].label.max()*[parameters.iloc[i,:]], ignore_index=True)
            if i == len(labels)-1:
                # pd.concat([rep,properties_table], axis = 1, ignore_index=True)
                properties_table = properties_table.reset_index(drop= True)
                properties_table[parameters.columns.str.replace(' ','')] = rep[parameters.columns]

        if distances == True:
            # get all centroid coordinates
            coords = np.column_stack([properties_table['centroid-0'], properties_table['centroid-1'], properties_table['centroid-2']])
            # make kd tree with all coords
            kdt = KDTree(coords)
            md_features = []
            for i, row in properties_table.iterrows():
                    z = row['centroid-0']
                    y = row['centroid-1']
                    x = row['centroid-2']

                    coord = np.array([z, y, x])
                    d, _ = kdt.query(coord, k=2)

                    md_features.append(d[1])

            properties_table['nn_distance'] = md_features
        # measure minimal distance of feature to surface
        if surface != None:
            kdtS = KDTree(surface[0])
            md_surface = []
            for i, row in properties_table.iterrows():
                    z = row['centroid-0']
                    y = row['centroid-1']
                    x = row['centroid-2']

                    coord = np.array([z, y, x])
                    d, _ = kdtS.query(coord, k=1)

                    md_surface.append(d)

            properties_table['ns_distance'] = md_surface


    return properties_table

def summarize_features(properties_table):
    #summerizes all sampled images. Mean and standard deviation of features is computed.
    #input: pd.DataFrame of properties table computed with "measure_features"
    #output: pd.DataFrame
    pt_sum = pd.DataFrame()
    features = ['area', 'nn_distance', 'ns_distance']

    ID = properties_table.drop(properties_table.columns[properties_table.columns.str.contains('centroid')], axis=1)
    ID = ID.drop(features + ['image','label'], axis=1)

    for i in pd.unique(properties_table.image):
        pt_sub = properties_table[properties_table.image == i]
        pt_sub = pt_sub[features]
        pt_sub_mean =  pt_sub.mean()
        pt_sub_mean = pd.DataFrame(pt_sub_mean.add_suffix('_mean'))

        pt_sub_std = pt_sub.std()
        pt_sub_std = pd.DataFrame(pt_sub_std.add_suffix('_std'))

        # feature_sub = pd.DataFrame([pt_sub_mean, pt_sub_std])
        feature_sub = pt_sub_mean.T.merge(pt_sub_std.T, right_index=True, left_index=True)
        feature_sub['n_labels'] = properties_table[properties_table.image == i].label.max()

        pt_sum = pt_sum.append(feature_sub, ignore_index=True)
    pt_sum = ID.drop_duplicates(ignore_index=True).merge(pt_sum, right_index=True, left_index=True)
    return pt_sum
