#analyse
import pandas as pd
import numpy as np
import plotly.express as px
import plotly.io as pio
import plotly.graph_objects as go
import scipy.signal as sp

from matplotlib import pyplot as plt

# import open3d as o3d
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler


def cluster_pca(summerized_features,variable,n_components):
    #Input: summerize feature table, variable (string) of interest, number of principal components
    #Output: PCA as pd.Df and np.array

    #standarize data
    pf_sub = summerized_features[summerized_features['variable'].str.contains(variable)]
    pf_sub = pf_sub.dropna()
    pf_sub = pf_sub.reset_index()
    features = pf_sub.columns.tolist()
    del features[0:4]
    x = pf_sub[features]
    x = StandardScaler().fit_transform(x)
    y = pf_sub[['a1','b1']]

    #PCA
    pca = PCA(n_components=n_components)

    PC = pca.fit_transform(x)
    PCdf = pd.DataFrame(data = PC, columns=['PC1', 'PC2'])
    PCdf = pd.concat([PCdf, y], axis=1, ignore_index=True)
    PCdf.columns=['PC1', 'PC2','a1','b1']
    return PCdf, PC