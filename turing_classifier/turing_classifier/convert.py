#conversion to binary image
import pandas as pd
import numpy as np
import math
from skimage import data
from skimage.filters import gaussian
from matplotlib import pyplot as plt
from skimage.filters import threshold_otsu, threshold_mean
from scipy import ndimage as ndi

def pointcloud_to_image(imported_df, mesh = None):
    #takes output from import_pointcloud_from_comsol as input. First three columnes are coordinates of the pointcloud, n following are simulated data values from n parameter combinations.
    #optional scale and recenter the pointclouds mesh
    #returns n images, where n = number of parameter combinations
    geom = imported_df[['X', 'Y', 'Z']]
    values = imported_df.drop(['X','Y','Z'], axis = 1)
    print(str(values.shape[0]) + (' Points and ')+ str(values.shape[1])+ ' Parameter combinations are converted into '+ str(values.shape[1])+' image(s)!')

    # calculate size of image, transform coordinate system of data
    binX = max(pd.factorize(geom.X)[0])
    increment_x = abs(pd.factorize(geom.X)[1][0]-pd.factorize(geom.X)[1][1])
    offset_x = min(geom.X)


    binY = max(pd.factorize(geom.Y)[0])
    increment_y = abs(pd.factorize(geom.Y)[1][0]-pd.factorize(geom.Y)[1][1])
    offset_y = min(geom.Y)

    binZ = max(pd.factorize(geom.Z)[0])
    increment_z = abs(pd.factorize(geom.Z)[1][0]- pd.factorize(geom.Z)[1][1])
    offset_z = min(geom.Z)

    im = np.zeros([binX+1,binY+1,binZ+1])
    # compute image coordinates
    x_coords = np.round(((geom.X.values - offset_x)/ increment_x)).astype(int)
    y_coords = np.round(((geom.Y.values - offset_y)/ increment_y)).astype(int)
    z_coords = np.round(((geom.Z.values - offset_z)/ increment_z)).astype(int)
    
    mesh_scaled = None
    if mesh is not None:
        mx_coords = np.round(((mesh[0][:,0] - offset_x)/ increment_x)).astype(int)
        my_coords = np.round(((mesh[0][:,1] - offset_y)/ increment_y)).astype(int)
        mz_coords = np.round(((mesh[0][:,2] - offset_z)/ increment_z)).astype(int)
        mesh_scaled = (np.column_stack([mx_coords,my_coords,mz_coords]),mesh[1])
    
     
    if values.shape[1] > 1:
        list_of_images = []
        for i in range(values.shape[1]):
            # empty Image with right size
            im = np.zeros([binX+1,binY+1,binZ+1])
            conc = values.iloc[:,i]/values.iloc[:,i].max()
            im[x_coords, y_coords, z_coords] = conc
            list_of_images.append(im)
        if mesh is None:
            return list_of_images
        else:
            return list_of_images, mesh_scaled
        # return list_of_images if mesh is None else list_of_images, mesh_scaled
    else:
        conc = values.iloc[:,0]#/values.iloc[:,0].max()
        im[x_coords, y_coords, z_coords] = conc
        if mesh is None:
            return im
        else:
            return im, mesh_scaled
        # return list_of_images if mesh is None 
        # return im if mesh is None else im, mesh_scaled

def segment_features(image):
    #input: list of images or image
    #output: (list of) binarized and (list of) labled image(s)
    if type(image) == list:
        list_binary = []
        list_label = []
        for i in range(len(image)):
            thresh = threshold_otsu(image[i])
            binary = image[i] > thresh
            label, _ = ndi.label(binary)
            list_binary.append(binary)
            list_label.append(label)
        return list_binary, list_label
    else:
        thresh = threshold_otsu(image)
        binary = image[i] > thresh
        label, _ = ndi.label(binary)
        return binary, label
