#reading files from comsol
import pandas as pd
import numpy as np 
import trimesh as tri

def import_pointcloud_from_comsol(x):
    # imports data exported as a grid from COMSOL and returns a pandas dataframe
    # takes path to semicolon seperated file including COMSOL header as input!
    df = pd.read_csv(x,delimiter=';', skiprows = 8)
    df = df.rename(columns={list(df)[0]: "X"})
    df = df.fillna(0)
    return df

def import_pointcloud_and_mesh_from_comsol(pointcloud,mesh=None):
    # imports data exported as a grid from COMSOL and returns a pandas dataframe
    #optional: loads in mesh from stl aswell
    # takes path to semicolon seperated file including COMSOL header as input!
    df = pd.read_csv(pointcloud,delimiter=';', skiprows = 8)
    df = df.rename(columns={list(df)[0]: "X"})
    df = df.fillna(0)
    if mesh != None:
        mesh = tri.load(mesh)
        vertices = np.asarray(mesh.vertices)
        faces = np.asarray(mesh.triangles)
        surface = (vertices, faces)
    return df, surface