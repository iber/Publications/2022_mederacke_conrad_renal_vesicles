#!/usr/bin/env python
from setuptools import setup


setup(
   name='turing_classifier',
   version='0.1.0',
   author='Malte Mederacke',
   author_email='malte.mederacke@bsse.ethz.ch',
   packages=['turing_classifier'],
   scripts=[],
   url='',
   license='LICENSE.txt',
   description='Load pointcloud data from comsol, converts it to an image and does measurements of observed turing patterns',
   long_description=open('README.txt').read(),
   install_requires=[
       "pandas",
       "numpy",
       "plotly",
       "scipy",
       "scikit-image",
       "matplotlib",
       "sklearn",
       "napari",

   ],
)