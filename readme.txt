Code to reproduce all results for the paper 'Geometric Effects Positions Renal Vesicles During Kidney Development'
by Malte Mederacke, Lisa Conrad, Roman Vetter, Dagmar Iber

https://www.biorxiv.org/content/10.1101/2022.08.30.505859v1



Software to open and run the files:

.m Matlab_R2020b
.ipynb Jupiter notebook python=3.9
.mph COMSOL Multiphysics 6.0




1. calc_displ_uni.m
matlab script to compute displacement fields used in COMSOL to simulate the growth of kidney explant cultures

2. integrate_along_normals.m
matlab script using COMSOL-Matlab livelink integrating 3D simulation data along surface normals of the UB

3. overlap_with_simulations.ipynb
jupyter notebook computing the simulated concentration inside and outside of RVs

4. project_to_epithelium_viz.ipynb
jupyter notebook file visualizing the integration done in 2.

5. requirements.txt
list of python packages required for the reproduction

6. segement_f_gfp.m
matlab script to segment kidney explant culture liveimage data

7. slice_viz.ipynb
jupyter notebook including voxilization and visualization of simulation data

8. smooth_norm_f.m
 matlab script smoothing explant image segementation and computing boundary normals

9. turing_classifier
package used for voxilization used in 7. 


10. L_stage_1.mph
Comsol model file for the first time step of the kidney culture growth simulations. Is the input for 11.

11. geom_set.m
Matlab script. Requires Comsol server with Matlab live link. Reads out the geometry of L_stage_1.mph and adapts it for the next time step.

Mesenchyme/UB
Segmented geometries from imaging data as input for 10.
	Bound_Aligned_Smooth
		contains coordinate files for boundaries. Output from 8.

12.curvature.mph
Comsol model file for the curvature simulations

13. angles_constantL.mph
Comsol model file for the kink simulations

3D_E12.5_kidney_Six2.mph
Comsol model file for simulations on E12.5 Kidney geometries

3D_kidney_2D_culture_48h.mph
Comsol model file for simulations on 3D explant culture endpoints
