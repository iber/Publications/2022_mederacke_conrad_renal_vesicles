%calculates the displacement field of segmented explant kidney cultures
%used for fig2B


% requires interparc and natsort

di = 1; % time step

%only positive growth: pos_only = 1
pos_only = 0
%exclude displacement at intersections of Epi and Mes
exV = 1
if exV == 1
    %load culture segmentations/contours
    load  Bound_Aligned_Smooth/Bound_Aligned_SmoothC;
    epi = boundSmoothC

     
    load Bound_Aligned_Smooth/Bound_Aligned_SmoothC;
    mes = boundSmoothC
end


%load contour
load Bound_Aligned_Smooth/Bound_Aligned_SmoothC;

% calculate displacement vectors between boundaries 
imax = 48;    % last time point

folder='DisplUni_oversample';
mkdir([folder '/' folder '_d' num2str(di) '/images/']);

displ={}
for n=1:di:imax-1
    bound = boundSmoothC{n};
    boundNext = boundSmoothC{n+1};

%lower resolution
    res = 300
    pb = interparc(res,bound(:,1),bound(:,2),'spline')
    pn = interparc(res*2,boundNext(:,1),boundNext(:,2),'spline')
    
    for i=1:length(pb)
        c = abs(pn - pb(i,:))
        c = c(:,1) + c(:,2);
        [~,ind] = min(c)
        int(i) = ind
    end
    pn = pn(int,:)
    vec = pn - pb

    %calcualte intersection point(s)of infered polygons
    [p1,p2] = polyxpoly(bound(:,1),bound(:,2),boundNext(:,1),boundNext(:,2)) 
    if length(p1) ~= 0
        p = [p1,p2]
    
    %find closest point of intersection
        int = []
        for i = 1:length(p)
            c = abs(pb - p(i,:));
            c = c(:,1) + c(:,2);
            [~,ind] = min(c)
            int(i) = ind
            int = sort(int)
        end

        % Determin direction of growth | 1 = growth, 0 = shrinkage
        %%%%%
        if pos_only == 1

        t = pb
        tt = t
        tt(:,3) = 0
        c = 1
        M = mean(bound)

        while c <= length(int)
           t = pb;
           t(:,3) = 0;

            if c < length(int)
            A = pb(int(c):int(c+1),:);
            B = pn(int(c):int(c+1),:);
            Am = floor(median(1:length(A)));
            vA = [A(Am,:), M];
            vB = [B(Am,:), M];


            else
                    A = pb([1:int(1),int(c)+1:length(pb)],:);
                    B = pn([1:int(1),int(c)+1:length(pn)],:);
            end

            
            if c < length(int)
                if norm([vA(1),vA(2)]-[vA(3),vA(4)]) > norm([vB(1),vB(2)]-[vB(3),vB(4)])
                    t(int(c):int(c+1),3) = 1;
                else
                    t(int(c):int(c+1),3) = 0 ;
                end
            elseif tt(int(c-1),3) == 0;
                    t(1:int(1),3) = 1;
                     t(int(c):end,3)= 1;
                else t(1:int(1),3) = 0 ;
                     t(int(c):end,3) =0;
            end  
            tt = tt+t(:,3)
            c = c+1
        end

    % if allow only for positiv growth, filter for 3rd column for 1
         if pos_only == 1
            pb = pb(tt(:,3)==1,[1,2])
            pn = pn(tt(:,3)==1,[1,2])
            vec =vec(tt(:,3)==1, :)
         end
     end
    end
    
   %exclude displacement at intersection of epi and mes
   if exV == 1
       
        mesi = mes{n}
        epii = epi{n}

        [p1,p2] = polyxpoly(mesi(:,1),mesi(:,2),epii(:,1),epii(:,2)) 
        p = [p1,p2]
        m = (p(1,:) + p(2,:)).'/2
        R = pdist(p, 'euclidean')   % radius 
        th = linspace(0,2*pi) ;
        cx = R*cos(th) + m(1) ;
        cy = R*sin(th) + m(2);

        inpb = inpolygon(pb(:,1),pb(:,2),cx,cy)
%         pb = pb(inpb == 0,:)
%         pn = pn(inpb == 0,:)
        vec(inpb == 1, :) = 0 
   end
    
    %Save displacement field and visualization
    displ{n}=[pb,vec]
    FileNameI=strcat('./', folder,  '/' , folder, '_d',num2str(di),'/displField_d',num2str(di),'_',num2str(n),'.txt');
    dlmwrite(FileNameI,displ{n});
    
    fig=figure('visible','off');
    xlim([0 max(bound(:,1))+50]);
    ylim([min(bound(:,2))-50 max(bound(:,2))+50]);
    hold on 
    plot(bound(:,1),bound(:,2),'r')
    plot(boundNext(:,1),boundNext(:,2),'b')
    for i=1:2:length(pb)
        if vec(i,1) ~= 0
%         plot([pb(i,1),pn(i,1)],[pb(i,2),pn(i,2)],"g-")
        q = quiver(pb(i,1),pb(i,2),vec(i,1),vec(i,2))
        q.MaxHeadSize = 0.7
        q
        end
    end

    hold off
    FileName=strcat('', folder, '/', folder, '_d',num2str(di),'/images/displField_d',num2str(di),'_',num2str(n),'.png');
    saveas(fig,FileName,'png');
end
