%% generating boundaries

clear all;
mkdir('Segment/Overlay');


%load image binaries
filenames = dir('../_Mes_Binary/B*tif')
filenames = {filenames.name}
k0 = 1; % first time point
kend = 48; %last time point
%% image segmentation

for k = k0:1:kend
    
    disp(['k=' num2str(k)])
 
    Filename = filenames{k}
    
    I = imread(strcat('../_Mes_Binary/',Filename)); % read in image
    I = imadjust(I); % increase contrast of images
    BW = imbinarize(I);  % binarizes image

%     CC = bwconncomp(BW); % returns connected components found in the binary image
%     numPixels = cellfun(@numel,CC.PixelIdxList); % counts number of pixels
%     [biggest,idx] = max(numPixels); % select biggest object
%     
%     l0=length(CC.PixelIdxList);
%     for i=1:l0
%         if i~=idx
%             BW(CC.PixelIdxList{i}) = 0;
%         end
%     end
%     
    segmentedC{k} = BW; % segmented contour
    imshow(BW); % show binary

    %% extract boundary
    
    Bound = bwboundaries(BW,'noholes'); % traces the exterior boundaries
    Bound = Bound{1};

    dim1 = size(I,1);
    dim2 = size(I,2);
%     Bound = clearBoundary(Bound, dim1, dim2); % remove points at boundary outside image dimensions
%     Bound = removeDiscont1(Bound); % remove discontinuity by rearanging points
    
    Xs=Bound(:,1);
    Ys=Bound(:,2);
    
    save('segmentedC','segmentedC');
    
    %% plot and export
    
    fig = figure('visible','off');
    imshow(I); % plot binary image
    hold on;
    plot(Ys,Xs,'Linewidth',1,'Color','r'); % plot boundary
    hold off;
    
%     name = strcat('Segment/Overlay/20171103_pos4_GFP_T0',num2str(k),'.png');
%     saveas(fig,name,'png');
    
    name = strsplit(Filename,'.');
    name = strcat('Segment/',name{1},'.txt') 
    dlmwrite(name,Bound);
    
end
