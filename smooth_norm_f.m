%% smooth boundaries and calculate normals to the boundaries

clear all;

pointSpline = 15;

imax = 48;    % last time point
boundSmoothC = cell(imax,1);

mkdir('Bound_Aligned_Smooth/images');
filenames = dir('Segment/*txt')
filenames = {filenames.name}
%% calculate splines and derivatives

interpC = cell(imax,1);
derC = cell(imax,1);
normalC = cell(imax,1);

for n = 1:1:imax
    % load boundaries
    name = strcat('Segment/',filenames{n});
    cur1 = dlmread(name)'; 
    %scale pixels to microns
    cur1 = cur1/1.5387; %1.5387 pixels per micron

    cur1p = cur1;    
        
    nP=length(cur1);
    points=[1:1:nP];
   
    nInterv = round(nP/pointSpline); %number of splines
    
    interpFx = spap2(nInterv,3,points,cur1(1,:)); % calculate spline
    interpFy = spap2(nInterv,3,points,cur1(2,:));
    interpC{n,1} = interpFx;                      % store spline   
    interpC{n,2} = interpFy;
    
    derC{n,1}=fnder(interpFx,1); % calculate first order of spline
    derC{n,2}=fnder(interpFy,1); 
end

clear bound;

%% calculate and store normal to the surface

for n=1:1:imax
    disp(['n=' num2str(n)])   
    name = strcat('Segment/',filenames{n});
    cur1 = dlmread(name)'; 
    cur1p = cur1;    
    
    nP=length(cur1);   % number of points to be calculated,
                        ... number of point in the original curve
    
    norm = zeros(nP,2);
    bound = zeros(nP,2);
    for j=1:nP  % tabulate normal field & boundary
        bound(j,1)=fnval(interpC{n,1},j);
        bound(j,2)=fnval(interpC{n,2},j);
                
        tx=fnval(derC{n,1},j); %calculate and store normal
        ty=fnval(derC{n,2},j);
        nn=(tx^2+ty^2)^0.5;
        norm(j,1)=-ty/nn;
        norm(j,2)=tx/nn;
    end
    normalC{n}=norm;
    boundSmoothC{n}=bound;
    size(boundSmoothC{n});
    
    % plot normals
    fig=figure('visible','off');
    plot(bound(:,1),bound(:,2),'Color','r'); 
    xlim([0 max(bound(:,1))+10]);
    ylim([min(bound(:,2))-10 max(bound(:,2))+10]);
    hold on
    cur1 = cur1';
    plot(cur1(:,1),cur1(:,2),'Color','g');  
    vl=3; %vector length
    for j = 1:1:nP %plot displ field
             x = [bound(j,1),bound(j,1)+vl*norm(j,1)];
             y = [bound(j,2),bound(j,2)+vl*norm(j,2)];
             plot(x,y);
    end
    hold off
        
    FileName=strcat('./Bound_Aligned_Smooth/images/Bound_Aligned_Smooth_', num2str(n),'.png');
    saveas(fig,FileName,'png');
    
    % limit number of points in the curve to 999
    bound=boundSmoothC{n};
    if length(boundSmoothC{n})>999
        bound=interparc(999,bound(:,1),bound(:,2),'lin');
        bound=bound';
    end
    oubnd = flip(bound(:,1:450))
    FileName=strcat('./Bound_Aligned_Smooth/BoundSmoothSt_', num2str(n),'.txt');
    dlmwrite(FileName, bound);
end
 
save('Bound_Aligned_Smooth/Bound_Aligned_SmoothC.mat', 'boundSmoothC');
save('Bound_ALigned_Smooth/normalC.mat', 'normalC');
% 
% close all;