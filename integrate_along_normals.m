
%requires COMSOL server with matlab livelink


model = mphload('3D_kidney_Six2_invivodif2.mph')


% %read normals either from Plot..
% model.result("pg6")
% n= mphplot(model,"pg6")
% n{1,1}{1}


%or from file (prefered)
n = readtable('./normals_ub.txt','HeaderLines', 8);
n = n{:,:};
%split UB vertices
sc = n(:,[1:3]);

%and normal vectors
no = n(:,[4:6]);

%define steps and distance over which to normalize
coord = {};
for i=[1:1:40]
coord{i} = sc+(no)*(i*1) ;
end

%evaluate concentration along the normals
for i=1:length(coord)
v = mphinterp(model, 'v', 'coord', coord{i}.', 'dataset', 'dset1','t',[7200])
coord{i} = cat(2,coord{i},v.');
end
coord = cellfun(@(c) fillmissing(c,'constant',0), coord,'UniformOutput',false)


%summ along all sampling points
all_val = []
for i=1:length(coord)
   all_val = cat(2,all_val,coord{i}(:,4))
end
non_zero = sum(all_val~=0,2);
sumv = coord{1}(:,4);
for i=2:length(coord)
sumv = sumv + coord{i}(:,4);
sumv(i) = sumv(i)/non_zero(i)
end
sum_surf = cat(2,sc,sumv)

%safe file
writematrix(sum_surf,'sum_surf.txt')

% input for project_to_epithelium_viz.ipynb

%some visualization for testing

for i=1:10:length(coord)
    hold on 
    % quiver3(n(:,1),n(:,2),n(:,3),n(:,4),n(:,5),n(:,6))
    cT = coord{i}
    scatter3(cT(:,1),cT(:,2),cT(:,3))
    hold off 
end


